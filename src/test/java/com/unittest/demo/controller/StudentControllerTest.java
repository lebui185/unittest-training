package com.unittest.demo.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unittest.demo.model.Student;
import com.unittest.demo.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
        MediaType.APPLICATION_JSON.getType(),
        MediaType.APPLICATION_JSON.getSubtype(),
        Charset.forName("utf8")
    );
    
    public static final MediaType APPLICATION_HAL_JSON_UTF8 = new MediaType(
		org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8.getType(),
		org.springframework.hateoas.MediaTypes.HAL_JSON_UTF8.getSubtype(),
        Charset.forName("utf8")
    );

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentRepository studentRepositoryMock;

    @InjectMocks
    private StudentController studentController;

    @Before
    public void init () {
    }


    @Test
    public void findAll_StudentsFound_ShouldReturnFoundStudentEntries () throws Exception {
        Student first  = new Student(1l, "Bob", "A1234567");
        Student second = new Student(2l, "Alice", "B1234568");

        when(studentRepositoryMock.findAll()).thenReturn(Arrays.asList(first, second));

        mockMvc.perform(get("/student"))
               .andExpect(status().isOk())
               .andExpect(content().contentType(APPLICATION_JSON_UTF8))
               .andExpect(jsonPath("$", hasSize(2)))
               .andExpect(jsonPath("$[0].id", is(1)))
               .andExpect(jsonPath("$[0].name", is("Bob")))
               .andExpect(jsonPath("$[0].passportNumber", is("A1234567")))
               .andExpect(jsonPath("$[1].id", is(2)))
               .andExpect(jsonPath("$[1].name", is("Alice")))
               .andExpect(jsonPath("$[1].passportNumber", is("B1234568")));

        verify(studentRepositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void retrieveStudent_StudentFound_ShouldReturnFoundStudentEntry() throws Exception {
    	Optional<Student> studentOptional = Optional.of(new Student(1l, "Bob", "A1234567"));
    	
        when(studentRepositoryMock.findById(1l)).thenReturn(studentOptional);
        
        mockMvc.perform(get("/student/1"))
		       .andExpect(status().isOk())
		       .andExpect(content().contentType(APPLICATION_HAL_JSON_UTF8))
		       .andExpect(jsonPath("$.name", is("Bob")))
		       .andExpect(jsonPath("$.passportNumber", is("A1234567")))
		       .andExpect(jsonPath("$._links.all-student.href", is("http://localhost/student")))
		       ;
        
        verify(studentRepositoryMock, times(1)).findById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void retrieveStudent_StudentNotFound_ShouldReturnNotFound() throws Exception {
    	final Optional<Student> studentOptional  = Optional.empty();
    	
        when(studentRepositoryMock.findById(1l)).thenReturn(studentOptional);
        
        mockMvc.perform(get("/student/1"))
		       .andExpect(status().isNotFound())
		       ;
        
        verify(studentRepositoryMock, times(1)).findById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void createStudent_ValidationPassed_ShouldReturnCreated() throws Exception {
    	final Student student = new Student(1l, "Bob", "A1234567");
    	final String requestBody = asJsonString(student);
    	
        when(studentRepositoryMock.save(Mockito.any(Student.class))).thenReturn(student);
    	
        mockMvc.perform(post("/student")
        		.content(requestBody)
        		.contentType(APPLICATION_JSON_UTF8))
		       .andExpect(status().isCreated())
		       ;
        
        verify(studentRepositoryMock, times(1)).save(Mockito.any(Student.class));
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void createStudent_ValidationFailed_ShouldReturnBadRequest() throws Exception {
    	final String nameWith101Char = createStringWithLength('c', 101);
    	final String passportWith16Char = createStringWithLength('c', 16);
    	final Student student = new Student(nameWith101Char, passportWith16Char);
    	final String requestBody = asJsonString(student);
    	
        mockMvc.perform(post("/student")
        		.content(requestBody)
        		.contentType(APPLICATION_JSON_UTF8))
		       .andExpect(status().isBadRequest())
		       ;
        
        verify(studentRepositoryMock, never()).save(Mockito.any(Student.class));
    }
    
    @Test
    public void updateStudent_ValidationPassed_ShouldReturnUpdatedStudentEntry() throws Exception {
    	final Optional<Student> studentOptional  = Optional.of(new Student(1l, "Bob", "A1234567"));
    	final String requestBody = asJsonString(studentOptional.get());
    	
        when(studentRepositoryMock.findById(1l)).thenReturn(studentOptional);
    	
        mockMvc.perform(put("/student/1")
        		.content(requestBody)
        		.contentType(APPLICATION_JSON_UTF8))
        		.andExpect(status().isOk())
        		.andExpect(content().contentType(APPLICATION_JSON_UTF8))
        		.andExpect(jsonPath("$.id", is(1)))
        		.andExpect(jsonPath("$.name", is("Bob")))
        		.andExpect(jsonPath("$.passportNumber", is("A1234567")))
		        ;
        
        verify(studentRepositoryMock, times(1)).findById(1l);
        verify(studentRepositoryMock, times(1)).save(Mockito.any(Student.class));
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void updateStudent_StudentNotFound_ShouldReturnNotFound() throws Exception {
    	final Student student = new Student(1l, "Bob", "A1234567");
    	final String requestBody = asJsonString(student);
    	
        when(studentRepositoryMock.findById(1l)).thenReturn(Optional.empty());
    	
        mockMvc.perform(put("/student/1")
        		.content(requestBody)
        		.contentType(APPLICATION_JSON_UTF8))
        		.andExpect(status().isNotFound())
		        ;
        
        verify(studentRepositoryMock, times(1)).findById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void updateStudent_ValidationFailed_ShouldReturnBadRequest() throws Exception {
    	final String nameWith101Char = createStringWithLength('c', 101);
    	final String passportWith16Char = createStringWithLength('c', 16);
    	final Student student = new Student(nameWith101Char, passportWith16Char);
    	final String requestBody = asJsonString(student);
    	
        mockMvc.perform(put("/student/1")
        		.content(requestBody)
        		.contentType(APPLICATION_JSON_UTF8))
		        .andExpect(status().isBadRequest())
		        ;
        
        verify(studentRepositoryMock, never()).save(Mockito.any(Student.class));
    }
    
    @Test
    public void deleteStudent_StudentFound_ShouldReturnOk() throws Exception {
        mockMvc.perform(delete("/student/1"))
		        .andExpect(status().isOk())
		        ;
        
        verify(studentRepositoryMock, times(1)).deleteById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    @Test
    public void deleteStudent_StudentNotFound_ShouldReturnNotFound() throws Exception {
    	
    	doThrow(EmptyResultDataAccessException.class).when(studentRepositoryMock).deleteById(1l);
    	
        mockMvc.perform(delete("/student/1"))
		       .andExpect(status().isNotFound())
		       ;
        
        verify(studentRepositoryMock, times(1)).deleteById(1l);
        verifyNoMoreInteractions(studentRepositoryMock);
    }
    
    public static String asJsonString (final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private static String createStringWithLength(char c, int count) {
    	StringBuilder stringBuilder = new StringBuilder(count);
        for(int i = 0; i < count; i++) {
            stringBuilder.append(c); 
        }
        return stringBuilder.toString();
    }
}

